*****************************************************************
XuC and XuC_mgt - applications web ccmanager, agent and assistant
*****************************************************************

Basic checks
============

XuC overview page
-----------------
XuC overview page available at @XUC_IP:PORT, usually @SERVER_IP:8090. You have to check if the
"Internal configuration cache database" contains agents, queues etc.

XuC sample page
---------------

XuC sample page available at @XUC_IP:PORT/sample, usually @SERVER_IP:8090/sample. You can use this
page to check user login and other API functions. CCManager, agent and assistant web use functions
available on the sample page.

XiVO Assistant
==============

Call history is missing
-----------------------

If the call history is missing in the XiVO Assistant interface, check that:
* you have properly configured the user *xuc* with all the right in the Configuration Management interface
* the XuC is able to join the Recording Server
